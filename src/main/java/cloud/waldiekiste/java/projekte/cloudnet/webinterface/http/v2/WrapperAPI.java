/*
 * Copyright (c) 2018.
 * Creative Commons Lizenzvertrag
 * CloudNet-Service-WebSocket-Extension von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International Lizenz.
 */

package cloud.waldiekiste.java.projekte.cloudnet.webinterface.http.v2;

import cloud.waldiekiste.java.projekte.cloudnet.webinterface.http.v2.utils.*;
import de.dytanic.cloudnet.lib.user.User;
import de.dytanic.cloudnet.lib.utility.document.Document;
import de.dytanic.cloudnet.web.server.handler.MethodWebHandlerAdapter;
import de.dytanic.cloudnet.web.server.util.PathProvider;
import de.dytanic.cloudnet.web.server.util.QueryDecoder;
import de.dytanic.cloudnetcore.CloudNet;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;

import java.util.ArrayList;
import java.util.List;

public final class WrapperAPI extends MethodWebHandlerAdapter {

    public WrapperAPI(CloudNet cloudNet) {
        super("/cloudnet/api/v2/wrapper");
        cloudNet.getWebServer().getWebServerProvider().registerHandler(this);
    }

    @SuppressWarnings( "deprecation" )
    @Override
    public FullHttpResponse get(ChannelHandlerContext channelHandlerContext, QueryDecoder queryDecoder,
                                PathProvider pathProvider, HttpRequest httpRequest) {
        FullHttpResponse fullHttpResponse = new DefaultFullHttpResponse(httpRequest.getProtocolVersion(),
                HttpResponseStatus.OK);
        fullHttpResponse = HttpUtil.simpleCheck(fullHttpResponse,httpRequest);
        User user = HttpUtil.getUser(httpRequest);
        switch (RequestUtil.getHeaderValue(httpRequest, "-Xmessage").toLowerCase()) {
            case "wrappers":{
                Document document = new Document();
                document.append("response", CloudNet.getInstance().getWrappers().keySet());
                return ResponseUtil.success(fullHttpResponse,true, document);
            }
            case "warpperinfos":{
                if(!UserUtil.hasPermission(user,"*","cloudnet.web.wrapper.item.*")){
                    return ResponseUtil.permissionDenied(fullHttpResponse);
                } else {
                    List<String> wrappers = new ArrayList<>();
                    CloudNet.getInstance().getWrappers().values().forEach(t -> wrappers.add(JsonUtil.getGson().toJson(t)));
                    Document resp = new Document();
                    resp.append("response", wrappers);
                    return ResponseUtil.success(fullHttpResponse, true, resp);
                }
            }
            default:{
                return ResponseUtil.xMessageFieldNotFound(fullHttpResponse);
            }
        }
    }
    @Override
    public FullHttpResponse options(ChannelHandlerContext channelHandlerContext, QueryDecoder queryDecoder,
                                    PathProvider pathProvider, HttpRequest httpRequest) {
        return ResponseUtil.cross(httpRequest);
    }
}
